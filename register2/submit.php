<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$facultate = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";
$pattern ="";
$url="https://www.facebook.com/";

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if (isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}
// validare
if(empty($firstname) || empty($lastname) || empty($facultate) || empty($phone) || empty($email) || empty($cnp)|| empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check))
{
	$error = 1;
	$error_text = "One or more fields are empty!";
}

if(strlen($firstname) < 3 || strlen($lastname) < 3){
	$error = 1;
	$error_text = "First or Last name is shorter than expected!";
}


if(strlen($firstname) > 20 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "First or Last name is longer than expected!";
}

if(strlen($facultate) < 3 || strlen($facultate) > 30) 
{
	$error=1;
	$error_text = 'Dar ce facultate faci boss';
}

if(strlen($question) < 15){
	$error = 1;
	$error_text = "Question is shorter than expected!";
}


if (is_numeric($firstname)||is_numeric($lastname) ) {
	$error = 1;
	$error_text = "First or Last name is not valid";

}

if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Phone number is not valid";
}

if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) 
{
	$error = 1;
	$error_text ="E-Mail address is not valid";
}

if(strlen($cnp >13) && strlen($cnp) < 13)  
{
	$error = 1;
	$error_text = "CNP must contain 13 numbers";

 /*if(!preg_match($pattern[1-6][0-9](13),$cnp))
 {
 	$error = 1;
	$error_text = "CNP must begin with numbers 1-6";*/ //am incercat si aici

 

 if (filter_var($url, FILTER_VALIDATE_URL)) {
    echo("facebook link  is valid");
} else {
    echo("facebook link is not valid");
}

 if ($captcha_generated!=$captcha_inserted) {
$error = 1;
	$error_text = "Captcha code is not valid";
}

// 50 entry limit - nu imi merge , dar am incercat 
  /* mysql_connect("localhost", "root", "");
        mysql_select_db("f.r.f");

        $sql = "select * from database_users";
        $query = mysql_query($sql);

        if(mysql_num_rows($query) < 50)
        {
            while($r = mysql_fetch_array($query))
            {
            
   
            }
        }
        else
        {
                echo "Sorry, you have reached the user account limit.";
        }
*/
try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,facultate,phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:facultate,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else

    echo "Succes";
}